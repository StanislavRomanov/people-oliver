'use strict';

angular
  .module('HouseToolsApp', [
    'ngSanitize',
    'smart-table',
    'ui.router',
    'ui.select',
    'ngFileUpload'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/people");
    $stateProvider
      .state('people', {
        url: "/people",
        templateUrl: "js/house-people/house-people.tpl.html",
        controller: 'HousePeopleCtrl'
      })
      .state('library', {
        url: "/library",
        templateUrl: "js/house-library/house-library.tpl.html",
        controller: 'HouseLibraryCtrl',
        params: {
          category: null
        }
      })
      .state('new-entry', {
        url: "/new-entry",
        templateUrl: "js/house-library/entry.tpl.html",
        controller: 'EntryCtrl'
      })
      .state('entry', {
        url: "/entry/:category/:id",
        templateUrl: "js/house-library/entry.tpl.html",
        controller: 'EntryCtrl',
        params: {
          entry: null,
          id: null,
          category: null
        }
      });
  });