angular.module('HouseToolsApp')
  .directive('imageDimensions', function() {
    return {
      restrict: 'EA',
      scope: {
        'imageDimensions': '='
      },
      link: function(scope, element, attrs, ctrl) {
        element.on('load', function() {
          scope.$apply(function() {
            scope.imageDimensions(element[0].naturalHeight, element[0].naturalWidth);
          });
        });

      }
    }
  });