'use strict';

/**
 *  Add/View/Edit entry controller
 * @param $scope
 * @param $state
 * @param $stateParams
 * @param Asset
 * @param ColorScheme
 */
function EntryCtrl($scope, $state, $stateParams, Asset, ColorScheme) {
  $scope.showNotes = false;
  $scope.somethingChanged = false;
  $scope.categories = [
    'Documents',
    'Site text',
    'Images',
    'Graphics',
    'Colours'
  ];
  $scope.width = 0;
  $scope.height = 0;
  $scope.dimensions = function(height, width) {
    $scope.width = width;
    $scope.height = height;
  };

  if($stateParams.entry) {
    $scope.entry = $stateParams.entry;

    if($scope.entry.notes) {
      $scope.showNotes = true;
    }

    $scope.$watch('entry', function(oldval, newval) {
      if(oldval) {
        $scope.somethingChanged = true;
      }
    });
  } else if($stateParams.id && $stateParams.category) {
    if($stateParams.category == 'graphics') {
      Asset.graphics.getAsset($stateParams.id)
        .then(function(response) {
          $scope.entry = response.data;
        })
        .catch(function(err) {
          console.error(err);
        });
    }
    if($stateParams.category == 'colours') {
      Asset.colours.getAsset($stateParams.id)
        .then(function(response) {
          $scope.entry = response.data;
        })
        .catch(function(err) {
          console.error(err);
        });
    }
  }

  $scope.isSaveDisabled = function() {
    if($scope.somethingChanged) {
      return false;
    } else {
      return !$scope.entry || !$scope.entry.category ||
        !$scope.entry.context ||
        !$scope.entry.type ||
        !$scope.entry.tags ||
        !$scope.entry.file;
    }
  };

  $scope.buildImageName = function() {
    if (typeof $scope.entry.file == "string") {
      return Asset.baseAPI_uri + $scope.entry.file;
    } else {
      var imageExt = $scope.entry.file.name.split('.');
      imageExt = imageExt[imageExt.length - 1];
      return $scope.entry && $scope.entry.name ?
        $scope.entry.name.toLowerCase().replace(/ /g, '-') + '.' + imageExt
        : '';
    }
  };

  $scope.previewColor = function(entry) {
    return {
      "background-color": ColorScheme
        .getCSS3_HSLAString(entry.h, entry.s, entry.l, entry.a)
    };
  };

  $scope.options = {
    highlight: true
  };

  $scope.save = function() {
    if($scope.entry.category == 'Colours') {
      if ($scope.entry.id) {
        return Asset.colours.updateAsset($scope.entry)
          .then(function(response) {
            $state.go('library', {
              category: 'colours'
            });
          })
          .catch(function(err) {
            console.error(err);
          });
      } else {
        return Asset.colours.addAsset($scope.entry)
          .then(function(response) {
            $state.go('library', {
              category: 'colours'
            });
          })
          .catch(function(err) {
            console.error(err);
          });
      }
    }
    if($scope.entry.category == 'Graphics') {
      if ($scope.entry.id) {
        return Asset.graphics.updateAsset($scope.entry)
          .then(function(response) {
            $state.go('library', {
              category: 'graphics'
            });
          })
          .catch(function(err) {
            console.error(err);
          });
      } else {
        return Asset.graphics.addAsset($scope.entry)
          .then(function(response) {
            $state.go('library', {
              category: 'graphics'
            });
          })
          .catch(function(err) {
            console.error(err);
          });
      }
    }
  };

  $scope.remove = function() {
    if(window.confirm('Are you certain?')) {

      if($scope.entry.category == 'Colours') {
        return Asset.colours.deleteAsset($scope.entry.id)
          .then(function(response) {
            $state.go('library', {
              category: 'colours'
            });
          })
          .catch(function(err) {
            console.error(err);
          });
      }
      if($scope.entry.category == 'Graphics') {
        return Asset.graphics.deleteAsset($scope.entry.id)
          .then(function(response) {
            $state.go('library', {
              category: 'graphics'
            });
          })
          .catch(function(err) {
            console.error(err);
          });
      }
    }
  };

  $scope.leave = function() {
    if($scope.somethingChanged && window.confirm('Changes were not saved. Are you Sure?')) {
      $state.go('library', {
        category: $scope.entry ? $scope.entry.category.toLowerCase() : ''
      });
    } else {
      $state.go('library', {
        category: $scope.entry ? $scope.entry.category.toLowerCase() : ''
      });
    }
  };
}

angular.module('HouseToolsApp')
  .controller('EntryCtrl', ['$scope', '$state', '$stateParams', 'Asset', 'ColorScheme', EntryCtrl]);