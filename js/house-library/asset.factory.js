'use strict';

/**
 * Service for assets management
 * @param $http
 * @returns {{listAllAssets: Function, addAsset: Function, getAsset: Function, deleteAsset: Function, updateAsset: Function}}
 * @constructor
 */
function Asset($http, $q) {
  var assetFactory = {
    baseAPI_uri: 'http://52.17.175.185',
    graphics: {
      listAllAssets: function(query) {
        return $http.get(assetFactory.baseAPI_uri + '/api/assets/graphics', {
          name: query.name || '',
          context: query.context || '',
          category: query.category || '',
          type: query.type || '',
          tags: query.tags || '',
          order_by: query.order_by || 'name',
          ordering: query.ordering || 'desc',
          limit: query.limit || 30,
          offset: query.offset || 0
        });
      },
      addAsset: function(asset) {
        var newData = {
          name: asset.name,
          context: asset.context,
          category: asset.category,
          type: asset.type,
          tags: asset.tags,
          alt_text: asset.altText,
          caption: asset.caption,
          credit_text: asset.credit_text,
          credit_link: asset.credit_link,
          notes: asset.notes
        };
        if (asset.file && typeof asset.file != "string") {
          newData['file'] = asset.file;
        }

        return $http({
          method: 'POST',
          url: assetFactory.baseAPI_uri + '/api/assets/graphics/add',
          headers: {
            'Content-Type': undefined
          },
          data: newData,
          transformRequest: function (data, headersGetter) {
            var formData = new FormData();
            angular.forEach(data, function (value, key) {
              formData.append(key, value);
            });
            return formData;
          }
        });
      },
      getAsset: function(assetId) {
        return $http.get(assetFactory.baseAPI_uri + '/api/graphic/' + assetId);
      },
      deleteAsset: function(assetId) {
        return $http.delete(assetFactory.baseAPI_uri + '/api/graphic/' + assetId);
      },
      updateAsset: function(asset) {
        var newData = {
          name: asset.name,
          context: asset.context,
          category: asset.category,
          type: asset.type,
          tags: asset.tags,
          alt_text: asset.altText,
          caption: asset.caption,
          credit_text: asset.credit_text,
          credit_link: asset.credit_link,
          notes: asset.notes
        };
        if (asset.file && typeof asset.file != "string") {
          newData['file'] = asset.file;
        }

        return $http({
          method: 'POST',
          url: assetFactory.baseAPI_uri + '/api/graphic/' + asset.id,
          headers: {
            'Content-Type': undefined
          },
          data: newData,
          transformRequest: function (data, headersGetter) {
            var formData = new FormData();
            angular.forEach(data, function (value, key) {
              formData.append(key, value);
            });
            // var headers = headersGetter();
            // delete headers['Content-Type'];
            return formData;
          }
        });
      }
    },
    colours: {
      listAllAssets: function(query) {
        var deferred = $q.defer();
        $http.get(assetFactory.baseAPI_uri + '/api/assets/colours', {
          name: query.name || '',
          context: query.context || '',
          category: query.category || '',
          type: query.type || '',
          tags: query.tags || '',
          where: query.where || '',
          which: query.which || '',
          what: query.what || '',
          order_by: query.order_by || 'name',
          ordering: query.ordering || 'desc',
          limit: query.limit || 30,
          offset: query.offset || 0,
          format: 'json'
        })
          .then(function(response) {
            response.data.forEach(function(colour) {
              colour.h = parseInt(colour.h);
              colour.s = parseInt(colour.s);
              colour.l = parseInt(colour.l);
              colour.a = parseInt(colour.a);
            });
            deferred.resolve(response);
          })
          .catch(function(err) {
            deferred.reject(err);
          });
        return deferred.promise;
      },
      addAsset: function(asset) {
        return $http({
          method: 'POST',
          url: assetFactory.baseAPI_uri + '/api/assets/colours/add',
          headers: {
            'Content-Type': undefined
          },
          data: {
            name: asset.name,
            context: asset.where,
            where: asset.where,
            which: asset.which,
            what: asset.what,
            hex: asset.hex,
            h: asset.h,
            s: asset.s,
            l: asset.l,
            a: asset.a
          },
          transformRequest: function (data, headersGetter) {
            var formData = new FormData();
            angular.forEach(data, function (value, key) {
              formData.append(key, value);
            });
            return formData;
          }
        });
      },
      getAsset: function(assetId) {
        var deferred = $q.defer();
        $http.get(assetFactory.baseAPI_uri + '/api/colour/' + assetId)
          .then(function(response) {
            response.data.h = parseInt(response.data.h);
            response.data.s = parseInt(response.data.s);
            response.data.l = parseInt(response.data.l);
            response.data.a = parseInt(response.data.a);
            deferred.resolve(response);
          })
          .catch(function(err) {
            deferred.reject(err);
          });
        return deferred.promise;
      },
      deleteAsset: function(assetId) {
        return $http.delete(assetFactory.baseAPI_uri + '/api/colour/' + assetId);
      },
      updateAsset: function(asset) {
        return $http({
          method: 'POST',
          url: assetFactory.baseAPI_uri + '/api/colour/' + asset.id,
          headers: {
            'Content-Type': undefined
          },
          data: {
            name: asset.name,
            context: asset.where,
            where: asset.where,
            which: asset.which,
            what: asset.what,
            hex: asset.hex,
            h: asset.h,
            s: asset.s,
            l: asset.l,
            a: asset.a
          },
          transformRequest: function (data, headersGetter) {
            var formData = new FormData();
            angular.forEach(data, function (value, key) {
              formData.append(key, value);
            });
            // var headers = headersGetter();
            // delete headers['Content-Type'];
            return formData;
          }
        });
      }
    }
  };

  return assetFactory;
}

angular.module('HouseToolsApp')
  .factory('Asset', ['$http', '$q', Asset]);