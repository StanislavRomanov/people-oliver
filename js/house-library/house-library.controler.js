'use strict';

/**
 *  House library list controller
 * @param $scope
 * @param $state
 * @param $stateParams
 * @param Asset
 * @param ColorScheme
 * @constructor
 */
function HouseLibraryCtrl($scope, $state, $stateParams, Asset, ColorScheme) {

  $scope.library = [];
  $scope.category = '';

  /**
   * Function loads graphics assets and then show table with them
   */
  $scope.loadGraphicsAssets = function() {
    $scope.category = '';
    Asset.graphics.listAllAssets({})
      .then(function(response) {
        $scope.library = response.data;
        $scope.category = 'graphics';
      })
      .catch(function(err) {
        console.error(err);
      });
  };

  /**
   * Function loads colours assets and then show table with them
   */
  $scope.loadColoursAssets = function() {
    $scope.category = '';
    Asset.colours.listAllAssets({})
      .then(function(response) {
        $scope.library = response.data;
        $scope.category = 'colours';
      })
      .catch(function(err) {
        console.error(err);
      });
  };

  $scope.save = function(entry) {
    return Asset.colours.updateAsset(entry)
      .catch(function(err) {
        console.error(err);
      });
  };

  if($stateParams.category) {
    if($stateParams.category == 'colours') {
      $scope.loadColoursAssets();
    }
    if($stateParams.category == 'graphics') {
      $scope.loadGraphicsAssets();
    }
  } else {
    // Load graphics assets by default
    $scope.loadGraphicsAssets();
  }

  //  Function prevent default action by field
  $scope.stopDefault = function(event) {
    event.stopPropagation();
    event.preventDefault();
  };

  $scope.previewColor = function(entry) {
    return {
      "background-color": ColorScheme
        .getCSS3_HSLAString(entry.h, entry.s, entry.l, entry.a)
    };
  };

  $scope.toHSLA = function(entry) {
    entry.h = ColorScheme.getHueFromHex(entry.hex);
    entry.s = ColorScheme.getSaturationFromHex(entry.hex);
    entry.l = ColorScheme.getLightnessFromHex(entry.hex);
  };
}

angular.module('HouseToolsApp')
  .controller('HouseLibraryCtrl', ['$scope', '$state', '$stateParams', 'Asset', 'ColorScheme', HouseLibraryCtrl]);