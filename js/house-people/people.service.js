'use strict';

function People($http, $q) {
  return {
    query: function(query) {
      if(query) {
        if(!query.filter) {
          query.filter = {};
        }
      } else {
        query = { filter: {} };
      }
      return $http.post('/people', {
        filter: {
          name: query.filter.name || '',
          postcode: query.filter.postcode || '',
          professions: query.filter.profession || [],
          rooms: query.filter.room || [],
          practices: query.filter.practice || [],
          contacts: query.filter.contact || [],
          mailings: query.filter.mailing || [],
          transactions: query.filter.transaction || [],
          validations: query.filter.validation || [],
          added: query.filter.added || []
        },
        offset: query.offset || 0,
        limit: query.limit || 10
      });
    },
    get: function() {
      return $http.get('sample.json');
    }
  }
}

angular.module('HouseToolsApp')
.factory('People', ['$http', '$q', People]);