angular.module('HouseToolsApp')
  .directive('roomsFilter', function() {
    return {
      restrict: 'EA',
      scope: {
        filter: '='
      },
      templateUrl: 'js/house-people/rooms.tpl.html',
      controller: ['$scope', function($scope) {
        $scope.visible = false;
        $scope.roomsList = [
            {
              title: 'TBC',
              active: false
            },
            {
              title: 'Top Renter!',
              active: false
            },
            {
              title: 'Renter',
              active: false
            },
            {
              title: 'Schmoozee',
              active: false
            },
            {
              title: 'Good potential',
              active: false
            },
            {
              title: 'Some potential',
              active: false
            },
            {
              title: 'Little potential',
              active: false
            },
            {
              title: 'Tiny potential',
              active: false
            },
            {
              title: 'Blank',
              active: false
            },
            {
              title: 'Dropped',
              active: false
            }
          ];
        $scope.all = function() {
          $scope.roomsList.forEach(function(room) {
            room.active = true;
          });
        };
        $scope.none = function() {
          $scope.roomsList.forEach(function(room) {
            room.active = false;
          });
        };
        $scope.show = function () {
          $scope.visible = true;
        };
        $scope.filterApply = function(e) {
          $scope.visible = false;
          $scope.filter = $scope.roomsList.filter(function(room) {
            return room.active;
          }).map(function (room) {
            return room.active ? room.title : '';
          })
        };
        $scope.closeDialog = function(e) {
          $scope.visible = false;
        };
      }],
      link: function(scope, element, attrs, ctrl) {
        $(document).bind('click', function(event){
          var isChild = element
              .find(event.target)
              .length > 0;

          if (isChild)
            return;

          scope.$apply(function(){
            scope.visible = false;
          });
        });
      }
    }
  }).filter('rooms', function() {
    return function(people, rooms) {
      if(!people)
        return people;
      return people.filter(function(person) {
        if(!rooms.length) {
          return true;
        } else {
          return rooms.indexOf(person.rooms) != -1;
        }
      })
    };
  });