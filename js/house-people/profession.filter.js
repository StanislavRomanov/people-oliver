angular.module('HouseToolsApp')
  .directive('professionFilter', function() {
    return {
      restrict: 'EA',
      scope: {
        filter: '='
      },
      templateUrl: 'js/house-people/profession.tpl.html',
      controller: ['$scope', function($scope) {
        $scope.visible = false;
        $scope.professionList = [
          {
            title: 'CBT Therapist',
            active: false
          },
          {
            title: 'Child Therapist',
            active: false
          },
          {
            title: 'Coach',
            active: false
          },
          {
            title: 'Counsellor',
            active: false
          },
          {
            title: 'Manager',
            active: false
          },
          {
            title: 'Mediator',
            active: false
          },
          {
            title: 'Medicolegal',
            active: false
          },
          {
            title: 'NHS GP',
            active: false
          },
          {
            title: 'Not Found',
            active: false
          },
          {
            title: 'Other',
            active: false
          },
          {
            title: 'Private GP',
            active: false
          },
          {
            title: 'Psychiatric Assessments',
            active: false
          },
          {
            title: 'Psychiatrist',
            active: false
          },
          {
            title: 'Psychologist',
            active: false
          },
          {
            title: 'Psychology Assessments',
            active: false
          },
          {
            title: 'Psychotherapist',
            active: false
          },
          {
            title: 'Secretary',
            active: false
          },
          {
            title: 'Blank',
            active: false
          }
        ];
        $scope.all = function() {
          $scope.professionList.forEach(function(profession) {
            profession.active = true;
          });
        };
        $scope.none = function() {
          $scope.professionList.forEach(function(profession) {
            profession.active = false;
          });
        };
        $scope.show = function () {
          $scope.visible = true;
        };
        $scope.filterApply = function(e) {
          $scope.visible = false;
          $scope.filter = $scope.professionList.filter(function(profession) {
            return profession.active;
          }).map(function (profession) {
            return profession.active ? profession.title : '';
          })
        };
        $scope.closeDialog = function(e) {
          $scope.visible = false;
        };
      }],
      link: function(scope, element, attrs, ctrl) {
        $(document).bind('click', function(event){
          var isChild = element
              .find(event.target)
              .length > 0;

          if (isChild)
            return;

          scope.$apply(function(){
            scope.visible = false;
          });
        });
      }
    }
  }).filter('profession', function() {
    return function(people, professions) {
      if(!people)
        return people;
      return people.filter(function(person) {
        if(!professions.length) {
          return true;
        } else {
          return professions.indexOf(person.profession) != -1;
        }
      })
    };
  });