'use strict';

/**
 *  House people list controller
 * @param $scope
 * @param People
 * @constructor
 */
function HousePeopleCtrl($scope, People) {
  $scope.showSideMenu = false;
  $scope.people = [];
  $scope.filter = {
    name: '',
    postcode: ''
  };
  $scope.professionFilter = [];
  $scope.roomsFilter = [];
  $scope.practiceFilter = [];
  $scope.contactFilter = [];
  $scope.mailingFilter = [];
  $scope.transactionFilter = [];
  $scope.validationFilter = [];
  $scope.addedFilter = [];
  $scope.columns = {
    profession: true,
    profile: true,
    postcode: true,
    email: true,
    phone: true,
    greeting: true,
    roomsPotential: true,
    practicePotential: true,
    contact: false,
    mailing: false,
    transaction: false,
    validation: false,
    added: false
  };

  $scope.$watchGroup([
    'professionFilter',
    'roomsFilter',
    'practiceFilter',
    'contactFilter',
    'mailingFilter',
    'transactionFilter',
    'validationFilter',
    'addedFilter'
  ], function() {
    $scope.reloadPeople();
  }, false);

  $scope.reloadPeople = function() {
    var filter = {
      name: $scope.filter.name,
      postcode: $scope.filter.postcode,
      profession: $scope.professionFilter,
      room: $scope.roomsFilter,
      practice: $scope.practiceFilter,
      contact: $scope.contactFilter,
      mailing: $scope.mailingFilter,
      transaction: $scope.transactionFilter,
      validation: $scope.validationFilter,
      added: $scope.addedFilter
    };
    $scope.people = $scope.people || [];
    People.query({
      filter: filter,
      limit: 20,
      offset: 0
    })
      .then(function(people) {
        $scope.people = people.data;
      })
      .catch(function(err) {
        console.error(err);
      });
  };

  $scope.appendPeople = function(tableState) {
    var filter = {
      name: $scope.filter.name,
      postcode: $scope.filter.postcode,
      profession: $scope.professionFilter,
      room: $scope.roomsFilter,
      practice: $scope.practiceFilter,
      contact: $scope.contactFilter,
      mailing: $scope.mailingFilter,
      transaction: $scope.transactionFilter,
      validation: $scope.validationFilter,
      added: $scope.addedFilter
    };
    $scope.people = $scope.people || [];
    People.query({
      filter: filter,
      limit: 20,
      offset: $scope.people.length || 0
    })
      .then(function(people) {
        $scope.people = $scope.people.concat(people.data);
      })
      .catch(function(err) {
        console.error(err);
      });
  };
}

angular.module('HouseToolsApp')
  .controller('HousePeopleCtrl', ['$scope', 'People', HousePeopleCtrl]);