angular.module('HouseToolsApp')
  .directive('monthFilter', function() {
    return {
      restrict: 'EA',
      scope: {
        filter: '=',
        title: '@'
      },
      templateUrl: 'js/house-people/month.tpl.html',
      controller: ['$scope', function($scope) {
        $scope.visible = false;
        $scope.years = [];
        for(var i = new Date().getFullYear(); i > 1990 ; i--) {
          $scope.years.push({
            year: i,
            opened: false,
            active: false,
            months: [
              {
                title: 'Jan',
                active: false
              },
              {
                title: 'Feb',
                active: false
              },
              {
                title: 'Mar',
                active: false
              },
              {
                title: 'Apr',
                active: false
              },
              {
                title: 'May',
                active: false
              },
              {
                title: 'Jun',
                active: false
              },
              {
                title: 'Jul',
                active: false
              },
              {
                title: 'Aug',
                active: false
              },
              {
                title: 'Sep',
                active: false
              },
              {
                title: 'Oct',
                active: false
              },
              {
                title: 'Nov',
                active: false
              },
              {
                title: 'Dec',
                active: false
              }
            ]
          });
        }
        $scope.professionList = [
          {
            title: 'CBT Therapist',
            active: false
          },
          {
            title: 'Child Therapist',
            active: false
          },
          {
            title: 'Coach',
            active: false
          },
          {
            title: 'Counsellor',
            active: false
          },
          {
            title: 'Manager',
            active: false
          },
          {
            title: 'Mediator',
            active: false
          },
          {
            title: 'Medicolegal',
            active: false
          },
          {
            title: 'NHS GP',
            active: false
          },
          {
            title: 'Not Found',
            active: false
          },
          {
            title: 'Other',
            active: false
          },
          {
            title: 'Private GP',
            active: false
          },
          {
            title: 'Psychiatric Assessments',
            active: false
          },
          {
            title: 'Psychiatrist',
            active: false
          },
          {
            title: 'Psychologist',
            active: false
          },
          {
            title: 'Psychology Assessments',
            active: false
          },
          {
            title: 'Psychotherapist',
            active: false
          },
          {
            title: 'Secretary',
            active: false
          },
          {
            title: 'Blank',
            active: false
          }
        ];
        $scope.all = function() {
          $scope.years.forEach(function(year) {
            year.active = true;
            year.months.forEach(function(month) {
              month.active = true;
            });
          });
        };
        $scope.none = function() {
          $scope.years.forEach(function(year) {
            year.active = false;
            year.months.forEach(function(month) {
              month.active = false;
            });
          });
        };
        $scope.show = function () {
          $scope.visible = true;
        };
        $scope.filterApply = function(e) {
          $scope.visible = false;
          $scope.filter = [];

          $scope.years.forEach(function(year) {
            year.months.forEach(function(month) {
              if(month.active) {
                $scope.filter.push(year.year.toString() + month.title.toString().toUpperCase())
              }
            });
          });
        };
        $scope.closeDialog = function(e) {
          $scope.visible = false;
        };
        $scope.toggleYear = function(year) {
          year.active = !year.active;
          year.months.forEach(function(month) {
            month.active = year.active;
          });
        };
        $scope.toggleMonth = function(year, month) {
          var isYearActive = false;
          month.active = !month.active;
          year.months.forEach(function(month) {
            isYearActive = isYearActive || month.active;
          });
          year.active = isYearActive;
        };
      }],
      link: function(scope, element, attrs, ctrl) {
        $(document).bind('click', function(event){
          var isChild = element
              .find(event.target)
              .length > 0;

          if (isChild)
            return;

          scope.$apply(function(){
            scope.visible = false;
          });
        });
      }
    }
  }).filter('month', function() {
    return function(people, professions) {
      if(!people)
        return people;
      return people.filter(function(person) {
        if(!professions.length) {
          return true;
        } else {
          return professions.indexOf(person.profession) != -1;
        }
      })
    };
  });