'use strict';

/**
 * Service for transform colors between two schemes HSL <-> HEX
 * @returns {{toHEX: Function, getHueFromHex: Function, getSaturationFromHex: Function, getLightnessFromHex: Function}}
 * @constructor
 */
function ColorScheme() {
  return {
    /**
     * Transform HSL color to HEX
     * @param h - heu
     * @param s - saturation
     * @param l - lightness
     * @param a - alpha (not used)
     * @returns HEX value of color (e.g. #010101)
     */
    toHEX: function(h,s,l,a) {
      var Color = net.brehaut.Color;
      return Color(
        [
          'hsl(',
          h, ',',
          s, '%,',
          l, '%)'
        ].join('')
      ).toString();
    },

    /**
     * Function is create CSS3 hsla color string
     * @param h - hue [0-360]
     * @param s - saturation [0-100]
     * @param l - lightness [0-100]
     * @param a - alpha [0-100]
     * @returns {string}
     */
    getCSS3_HSLAString: function(h, s, l, a) {
      return [
        'hsla(',
        h, ',',
        s, '%,',
        l, '%,',
        parseInt(a) / 100, ')'
      ].join('');
    },

    /**
     * Extract Hue component from HEX value of color
     * @param hex
     * @returns {number} [0-360]
     */
    getHueFromHex: function(hex) {
      var Color = net.brehaut.Color;
      var hsla = Color(hex).toHSL();
      return Math.ceil(hsla.getHue());
    },
    /**
     * Extract Saturation component from HEX value of color
     * @param hex
     * @returns {number} [0-100]
     */
    getSaturationFromHex: function(hex) {
      var Color = net.brehaut.Color;
      var hsla = Color(hex).toHSL();
      return Math.ceil(hsla.getSaturation() * 100);
    },
    /**
     * Extract Lightness component from HEX value of color
     * @param hex
     * @returns {number} [0-100]
     */
    getLightnessFromHex: function(hex) {
      var Color = net.brehaut.Color;
      var hsla = Color(hex).toHSL();
      return Math.ceil(hsla.getLightness() * 100);
    }
  }
}

angular.module('HouseToolsApp')
  .factory('ColorScheme', [ColorScheme]);