angular.module('HouseToolsApp')
  .directive('sideMenu', function() {
    return {
      restrict: 'EA',
      scope: {
        visible: '='
      },
      templateUrl: 'js/common/side-menu.tpl.html',
      controller: ['$scope', function($scope) {
        $scope.showPeopleSubMenu = false;
        $scope.showHouseLibrarySubMenu = false;
        $scope.showPersonList = false;
        //  Reset to default state on hide sidebar
        $scope.$watch('visible', function(value) {
          if(!value) {
            $scope.showPeopleSubMenu = false;
            $scope.showPersonList = false;
          }
        });
      }],
      link: function(scope, element, attrs, ctrl) {

      }
    }
  });