angular.module('HouseToolsApp')
  .directive('lookPerson', function() {
    return {
      restrict: 'EA',
      scope: {
        show: '='
      },
      templateUrl: 'js/common/look-person.tpl.html',
      controller: ['$scope', function($scope) {
        $scope.persons = [
          {
            name: "Marvin O'Gravel",
            profession: "Psychiatrist"
          },
          {
            name: "George",
            profession: "Administrator"
          },
          {
            name: "Biffalo Buffington",
            profession: "NHS Psychiatrist"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          },
          {
            name: "Peter Plumpington",
            profession: "Counsellor"
          }
        ];
      }],
      link: function(scope, element, attrs, ctrl) {

      }
    }
  });